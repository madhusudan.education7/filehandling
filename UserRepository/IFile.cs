﻿using FileHandling.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileHandling.UserRepository
{
    internal interface IFile
    {
        void WriteContentsToFile(User user, string filename);
        List<string> ReadContentsFromFile(string filename);
        bool IsUsernameExists(string userName, string filename);
    }
}
