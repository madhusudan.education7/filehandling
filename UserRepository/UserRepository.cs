﻿using FileHandling.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileHandling.UserRepository
{
    internal class UserRepository:IUserRepository, IFile
    {
        List<User> users;

        public UserRepository()
        {
            users = new List<User>();
        }

        public List<string> ReadContentsFromFile(string filename)
        {
            List<string> rowValues = new List<string>();

            using (StreamReader sr = new StreamReader(filename))
            {
                //while(sr.Peek() >= 0){
                //    string line = sr.ReadLine();
                //    rowValues.Add(line);
                //}
                string rowLine;
                while((rowLine = sr.ReadLine()) != null)
                {
                    rowValues.Add(rowLine);
                }
            }return rowValues;
            //return rowValues;
        }

        public bool RegisterUser(User user)
        {
            users.Add(user);   
            string filename = "userDatabase.txt";
            /*WriteContentsToFile*//*(user, filename);*/
            bool isuserameexisits = IsUsernameExists(user.Name, filename);
            if (isuserameexisits)
            {
                return true;
            }
            else
            {
                WriteContentsToFile(user, filename);
                return false;
            }
            //    if (!isuserameexisits)
            //    {
            //    WriteContentsToFile(user, filename);
            //    return true;
            //}
            //else
            //{

            //}
            //users.Add(user);
            //return true;
            //return false;
        }

        public void WriteContentsToFile(User user, string filename)
        {
            using (StreamWriter sw = new StreamWriter(filename,true))
            {
                sw.WriteLine($"{user}");
            }
        }

       public bool IsUsernameExists(string userName, string filename)
        {
            bool isUserAvailable = false;
            try
            {
                using (StreamReader sr = new StreamReader(filename))
                {
                    string rowLine;
                    while ((rowLine = sr.ReadLine()) != null)
                    {
                        string[] rowSplittedValue = rowLine.Split(',');
                        foreach (string userIndividualvalue in rowSplittedValue)
                        {
                            if (userIndividualvalue == userName)
                            {
                                isUserAvailable = true;
                                break;
                            }
                        }
                    }
                }            
            }
            catch(Exception ex)
            {
                Console.WriteLine("File not found");
            }
            return isUserAvailable;
        }

  
    }
}
