﻿using FileHandling.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileHandling.UserRepository
{
    internal interface IUserRepository
    {
        bool RegisterUser(User user);

        //bool RegisterUser(User user, object value1, object value2, object value3);
    }
}
